"""
Fichier de formation autour de l'olgorithmique et des boucles de controle
"""

# Souvent un algoritme sert à simplifier des tâches répétitives
# Pour cela on a à disposition quelques mots-clés : for, if, while
# for permet d'assigner une valeur différente à une variable à chaque fois que l'on recommence une boucle
# sa syntaxe est for <variable> in <collection ordonnée de trucs>:
# exercice : créer une fontion qui prends une liste d'entier en argument et retourne une liste conteannt ces valeurs +1

# idem avec une disctionnaire en entrée

# exercice : compter les éléments en même temps qu'on les parcoure : le générateur enumerate

# un autre générateur : range (xrange pour python<3)

# différences entre liste et générateur

# if permet de tester si une condition est vraie avec d'executer un morceau de code
# sa syntaxe est : if <booléen>:
# on peut y ajouter : elif <booléen>:
# et else :
# exercice : créer une fonction qui divise un argument par un autre (en vérifiant que celui-ci n'est pas 0 !


# while permet d'executer une boucle tant qu'une condition n'est pas atteinte
# sa syntaxe est while <booléen> :
# attention ! cela veut dire que si la condition n'est jamais atteinte, la boucle ne s'arretera jamais !!
# exercice : écrire une fonction qui donne les diviseurs d'un nombre entier