# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

a = 1 # nombre entier : int
b = 1.2 # nombre flottant : float
c = [1, 2] # liste de truc : list
d = "1 2"  # chaîne de caractères : str
e = (1, 2) # liste de truc non éditable : tuple
f = {1, 2} # ensemble de truc : set
g = {"premier":1, "deuxieme":2} # ensemble de de truc avec des clés : dict
h = True # booléen : bool

print(a)
print(type(a))
print(c)
print(c[0]) # affiche le premier élément de c
print(c[-1]) # affiche le dernier élément de c
print(c[a])
l = [1,2,3,4,4]
print(l)
s = set(l)
print(s)
print(len(s))

# créer et écrire dans la console une liste contenant deux fois les valeurs de 1 à 5
m = [1,2,3,4,5]*2
print(m)
n=set(m)
# compter le nombre de valeurs uniques dans m
print(n)
print("nombre de valeurs uniques",len(n))

# écrire 80 fois # dans la console 
print("#"*80)

print('1/3 =', 1/3)
print('1/3 = %10.2f'%(1/3))
print('1/3000 = %10.2e'%(1/3000))
print("coucou\tcoucou")
print(r"coucou\tcoucou")
print(f"liste = {l}, {m}")

def fonction(nom, prenom="gilles", sexe="F"):
    """
    une fonction qui dit bonjour

    Parameters
    ----------
    nom : str
        nom de famille.
    prenom : str, optional
        prenom. The default is "gilles".
    sexe : str, optional
        sexe. The default is "F".

    Returns
    -------
    None.

    """
    print("bonjour", prenom, nom, "sexe", sexe)

fonction("dennetiere")
fonction("dennetiere", "david", "m")
fonction("dennetiere", prenom="david", sexe="m")
fonction("dennetiere", sexe="m", prenom="david")

# les opérateurs sont des fonctions paticulières :
print(3+2)
print(type(3+2)) # l'addition d'un int avec un autre int retourne un int
print(3<2) 
print(type(3<2)) # l'opérateur de comparaison retourne un booléen
# les opérateurs sont en fait définis comme ça :
print(int.__add__(3, 2))
# Allons explorer ce mécanisme et ce que cela signifie