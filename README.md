# CoursPython
Exercices d'apprentissage du Python
===================================

Partie 1 : variables et types
-----------------------------

fichiers : variables et types.py
Découvertes des principaux types en Python
Manipulation de variables
Première fonction

Partie 2 : structures de contrôle
---------------------------------

fichiers : boucle de controle.py
Découverte de if, for, while
Comparaison entre liste et générateur
Création d'une fonction crible d'ératostène

Partie 3 : Classe et méthodes
-----------------------------

fichiers : classes.py
Découverte des classes, des attributs et des méthodes
Manipulation des imports
Création d'une classe
Création d'un module

Partie 4 : import de librairies externe
---------------------------------------

Premier import explicite et wildcard
Complétion de code
Lecture de documentation
Création d'une libraire et import de celle-ci